from django.contrib import admin

from .models import Word, Vote, Comment

admin.site.register(Word)
admin.site.register(Vote)
admin.site.register(Comment)