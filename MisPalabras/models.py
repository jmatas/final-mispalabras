from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Word(models.Model):
    name = models.CharField(max_length=32)
    date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING, default=1)  # Usuario que ha añadido la palabra
    votes = models.IntegerField(default=0)

    def __str__(self):
        return str(self.name)

    def __author__(self):
        return str(self.author)

    def __date__(self):
        return self.date.strftime("%b %d, %Y")

class Vote(models.Model):
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)

    def __word__(self):
        return str(self.word)

    def __author__(self):
        return str(self.author)

    def __date__(self):
        return self.date.strftime("%b %d, %Y")


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    content = models.CharField(max_length=240)

    def __str__(self):
        return str(self.content)

    def __word__(self):
        return str(self.word)

    def __author__(self):
        return str(self.author)

    def __date__(self):
        return self.date.strftime("%b %d, %Y")


class Link(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    url = models.URLField()
    date = models.DateTimeField(auto_now=True)

    def __date__(self):
        return self.date.strftime("%b %d, %Y")

class TarjetaEmbebida(models.Model):
    image = models.URLField()
    content = models.TextField()
    link = models.ForeignKey(Link, default=None, on_delete=models.CASCADE)

    def __author__(self):
        return str(self.link.author)

    def __date__(self):
        return self.link.__date__



class AutoInfo(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date = models.DateTimeField(auto_now=True)
    content = models.TextField(default="")
    site = models.CharField(max_length=10, choices=(("Flickr", "Flickr"), ("RAE", "RAE"), ("Meme", "Meme")))