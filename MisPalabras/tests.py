

from django.test import TestCase
from django.test import Client
import xml.etree.ElementTree as ET
import requests

from MisPalabras.models import Word
# Create your tests here.

class Tests(TestCase):
    def test_page_home(self):
        c = Client()
        response = c.get("/1")
        self.assertEqual(response.status_code, 200)
        res_content = response.content.decode('utf-8')
        self.assertIn("<title>Inicio / Mis Palabras</title>", res_content)


    def test_page_word(self):
        c = Client()
        word = "cama"
        response = c.get("/" + word)
        self.assertEqual(response.status_code, 200)
        res_content = response.content.decode('utf-8')
        self.assertIn("<title>Word / Mis Palabras</title>", res_content)


    def test_page_profile(self):
        c = Client()
        response = c.get("/profile")
        print(response.status_code)
        self.assertEqual(response.status_code, 302)  # Al no haber iniciado sesión nos redirige para que lo hagamos


    def test_page_login(self):
        c = Client()
        response = c.get("/login")
        self.assertEqual(response.status_code, 200)
        res_content = response.content.decode('utf-8')
        self.assertIn("<title>Log in / Mis Palabras</title>", res_content)

    def test_page_signup(self):
        c = Client()
        response = c.get("/signup")
        self.assertEqual(response.status_code, 200)
        res_content = response.content.decode('utf-8')
        self.assertIn("<title>Sign up / Mis Palabras</title>", res_content)

    def test_page_help(self):
        c = Client()
        response = c.get("/help")
        self.assertEqual(response.status_code, 200)
        res_content = response.content.decode('utf-8')
        self.assertIn("<title>Help / Mis Palabras</title>", res_content)

