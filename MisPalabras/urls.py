from django.urls import path
from django.contrib.auth.views import LogoutView
from django.views.generic import RedirectView

from practicaFinal import settings

from . import views

urlpatterns = [
    path('profile/<str:username>', views.page_profile_logged, name="profile_logged"),
    path('profile', views.page_profile, name="profile"),
    path('login', views.page_login, name="login"),
    path('signup', views.page_signup, name="signup"),
    path('help', views.page_help, name="help"),
    path('logout', LogoutView.as_view(next_page="home"), name='logout'),
    path('format=xml', views.page_xml, name="xml"),
    path('format=json', views.page_json, name="json"),
    path('<int:num>', views.page_home, name="home"),
    path('<str:searched_word>', views.page_word, name="word"),
    path('<str:searched_word>/<str:action>', views.page_word_action, name="word_action"),
    path('', views.page_home_first, name="home")
]
