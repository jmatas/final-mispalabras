import re
import urllib.request
import xml.etree.ElementTree as ET
import json
import random
import string
import requests
from bs4 import BeautifulSoup
from django.utils.safestring import mark_safe

from MisPalabras.models import Word, Vote, Comment, Link, AutoInfo, TarjetaEmbebida
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, auth

WORD_HTML = """
<div class='word'>
    <div class='word-section info'>
        <div class='word-content'>
            <div class='content-section title'>
                <a href='{word_title_link}'>{word_title}</a>
            </div>
            <div class='content-section text'>
                <p>{word_description}</p>
            </div>
            <div class='content-section added'>
                <p class='author'>Añadido por {author} en {date}</p>
                <p class='votes'>Votos : {votes}</p>
            </div>
        </div>
        <div class='img'>
                <img src='{img}' alt='imagen'>
        </div>
    </div>
</div>
"""

COMMENT_HTML = """
<div class='comment'>
    <div class='comment-section comment-info '>
        <span class='author'>{author}</span>
        <span class='date'>{date}</span>
    </div>
    <div class='comment-section comment-text'>
        <p>{comment_text}</p>
    </div>
</div>
"""

PROFILE_WORD = """
<div class='word'>
    <div class='info'>
        <a href='{word_link}'>{word}</a>
        <p>{date}</p>
    </div>
    <p>{description}</p>
</div>
"""

PROFILE_COMMENT = """
<div class='comment'>
    <div class='info'>
        <a href='{word_link}'>{word}</a>
        <p>{date}</p>
    </div>
    <p class='comment-text'>{comment}</p>
</div>
"""

PROFILE_LINK = """
<div class='link'>
    <a href='/{word}'>{word}</a>
    <span>Link : {link}</span>
</div>
"""

PROFILE_LINK_TE = """
<div class='link'>
    <a href='/{word}'>{word}</a>
    <span>Link : {link}</span>
    <div class='embebida'>
        <div class='text'>
            <p>{text}</p>
        </div>
        <div class='img'>
            <img src='{img_link}'>
        </div>
    </div>
</div>
"""

PROFILE_AI = """
<div class='ai'>
    <div class='ai-content'>
        <a href='/{word}'>{word}</a>
        <div class='ai-content'>
            {content}
        </div>
    </div>
</div>
"""

WORD_FLICKR = """
    <img src='{img_url}'>
"""
WORD_RAE = """
    <p>{definition}</p>
"""

WORD_MEME = """
    <img src='{img_url}'>
"""

WORD_ADD_FORM = """
<form action='{link}'>
    <button class="add-btn">Add</button>
</form>
"""

WORD_MEME_FORM = """
<form action='{link}' method='POST'>
    <select name='meme-img'>
        <option value='op1'>Afraid to ask Andy</option>
        <option value='op2'>10 guy</option>
        <option value='op3'>Advice Doge</option>
    </select>
    <label for='meme-top'></label>
    <input type='text' id='meme-top' name='meme-top' placeholder='Top text'/>
    <label for='meme-bot'></label>
    <input type='text' id='meme-bot' name='meme-bot' placeholder='Bottom text'/>
    <button class="add-btn">Add</button>
</form>
"""

def page_help(request):
    if request.user.is_authenticated:
        context = {
            "username": request.user.username,
            "login_display": "display:none",
            "logout_display": "",
            "top_words_table": getTopWordsHTML()
        }
        template = loader.get_template("MisPalabras/html/help.html")
        response = template.render(context, request)
        return HttpResponse(response)
    else:
        context = {
            "username": "",
            "login_display": "",
            "logout_display": "display:none",
            "top_words_table": getTopWordsHTML()
        }
        template = loader.get_template("MisPalabras/html/help.html")
        response = template.render(context, request)

        return HttpResponse(response)


@csrf_exempt
def page_home_first(request):
    return page_home(request, 0)


@csrf_exempt
def page_home(request, num):
    if request.method == "GET":
        prev_page: int
        next_page: int
        if num < 1:
            prev_page = 0
        else:
            prev_page = num - 1
        if num < getNumberOfPages():
            next_page = num + 1
        else:
            return redirect("/" + str(getNumberOfPages() - 1))
        if request.user.is_authenticated:
            context = {
                "username": request.user.username,
                "login_display": "display:none",
                "logout_display": "",
                "top_words_table": getTopWordsHTML(),
                "table_body": getTableWordsHTML(num),
                "number_of_words": len(Word.objects.all()),
                "next_page": next_page,
                "prev_page": prev_page
            }
            template = loader.get_template("MisPalabras/html/home.html")
            response = template.render(context, request)
            return HttpResponse(response)
        else:
            context = {
                "username": "",
                "login_display": "",
                "logout_display": "display:none",
                "top_words_table": getTopWordsHTML(),
                "number_of_words": len(Word.objects.all()),
                "table_body": getTableWordsHTML(num),
                "next_page": next_page,
                "prev_page": prev_page
            }
            template = loader.get_template("MisPalabras/html/home.html")
            response = template.render(context, request)

            return HttpResponse(response)

    elif request.method == "POST":
        searched_word = request.POST["search"]
        return redirect("/" + searched_word)


@csrf_exempt
def page_login(request):
    if request.method == "GET":
        template = loader.get_template("MisPalabras/html/login.html")
        context = {
            "error_msg_dp": "display:none",
            "error_msg": "",
            "top_words_table": getTopWordsHTML()
        }
        respuesta = template.render(context, request)
        return HttpResponse(respuesta)

    elif request.method == "POST":
        print(request.POST)
        username = request.POST["username"]
        password = request.POST["password"]

        user = auth.authenticate(username=username, password=password)
        if user is not None:  # Usuario correcto
            auth.login(request, user)
            return redirect("home")
        else:
            template = loader.get_template("MisPalabras/html/login.html")
            context = {
                "error_msg_dp": "",
                "error_msg": "Invalid username or password",
                "top_words_table": getTopWordsHTML()
            }
            respuesta = template.render(context, request)
            return HttpResponse(respuesta)


def page_profile(request):
    if request.user.is_authenticated:
        user = request.user.username
        return redirect("/profile/" + user)
    else:
        return redirect("login")


def page_profile_logged(request, username):
    user = request.user
    context = {
        "username": username,
        "login_display": "display:none",
        "logout_display": "",
        "top_words_table": getTopWordsHTML(),
        "words": getProfileWordsHTML(user),
        "comments": getProfileCommentsHTML(user),
        "ai": getProfileAIHTML(user),
        "links": getProfileLinksHTML(user)
    }
    template = loader.get_template("MisPalabras/html/profile.html")
    response = template.render(context, request)
    return HttpResponse(response)


@csrf_exempt
def page_signup(request):
    if request.method == "GET":
        template = loader.get_template("MisPalabras/html/signup.html")
        context = {
            "top_words_table": getTopWordsHTML()
        }
        respuesta = template.render(context, request)
        return HttpResponse(respuesta)
    elif request.method == "POST":
        name = request.POST["username"]
        email = request.POST["email"]
        password = request.POST["password"]
        password2 = request.POST["password2"]
        error_code = check(name, email, password, password2)
        if error_code == 0:
            addUser(name=name, email=email, password=password)
            user = auth.authenticate(username=name, password=password)
            auth.login(request, user)
            return redirect("home")
        else:
            template = loader.get_template("MisPalabras/html/signup.html")
            context = {
                "error_msg_dp": "",
                "error_msg": getErrorMessage(error_code),
                "top_words_table": getTopWordsHTML()
            }
            respuesta = template.render(context, request)
            return HttpResponse(respuesta)


@csrf_exempt
def page_word(request, searched_word):
    if request.method == "GET":
        # Cargar información de la palabra sacándola de Wikipedia -> GET + wiki link
        word_name = str(searched_word).strip().lower()
        word_title = word_name.capitalize()
        word_info = getWikiText(searched_word)
        word_exists = (word_info != "Couldn't find any information of this word.")
        # Cargar HTML
        context = {
            "word": word_title,
            "word_description": word_info,
            "searched-word": searched_word,
            "top_words_table": getTopWordsHTML()
        }

        if request.user.is_authenticated:
            username = request.user.username
            context["username"] = username
            context["login_display"] = "display:none"
            context["logout_display"] = ""
            context["display_button_add"], context["display_button_vote"], context[
                "display_button_rmvote"] = getWordButtonContext(word_name, request.user)
        else:
            context["username"] = ""
            context["login_display"] = ""
            context["logout_display"] = "display:none"
            context["display_button_add"] = "display:none"
            context["display_button_vote"] = "display:none"
            context["display_button_rmvote"] = "display:none"

        if word_exists:
            context["image_source"] = getWikiImage(searched_word)  # Si la palabra existe buscamos la imagen
            if isAlreadyAdded(word_name):
                dbWord = Word.objects.all().get(name=word_title.lower())
                context["display_added"] = getAddedHTML(word_name)
                context["comments"] = getWordCommentsHTML(dbWord)
                context["links"] = ""
                context["flickr"] = getAIFlickrHTML(dbWord)
                context["rae"] = getAIRAEHTML(dbWord)
                context["meme"] = getAIMemeHTML(dbWord)
            else:
                context["display_comments"] = "display:none"
                context["display_ai"] = "display:none"
                context["display_links"] = "display:none"
                context["links"] = ""
        else:
            context["display_button_add"] = "display:none"
            context["display_button_vote"] = "display:none"
            context["display_button_rmvote"] = "display:none"
            context["display_comments"] = "display:none"
            context["display_ai"] = "display:none"
            context["display_links"] = "display:none"
            context["links"] = ""

        template = loader.get_template("MisPalabras/html/word.html")
        response = template.render(context, request)

        return HttpResponse(response)


@csrf_exempt
def page_word_action(request, searched_word: str, action: str):
    if request.user.is_authenticated:
        user = request.user
        if action == "add_link":
            link = request.POST["add-link"]
            addLink(searched_word, user, link)
        elif action == "add":
            addWord(searched_word, user)
        elif action == "vote":
            voteWord(searched_word, user)
        elif action == "remove":
            removeVoteWord(searched_word, user)
        elif action == "comment":
            text = request.POST["comment-text"]
            addComment(searched_word, user, text)
        elif action == "flickr":
            addFlickr(searched_word, user)
        elif action == "RAE":
            addRAE(searched_word, user)
            pass
        elif action == "meme":
            meme_img = request.POST["meme-img"]
            if meme_img == "op1":
                meme_img = "Afraid-To-Ask-Andy"
            elif meme_img == "op2":
                meme_img = "10-Guy"
            elif meme_img == "op3":
                meme_img = "Advice-Doge"
            top_text = request.POST["meme-top"]
            bottom_text = request.POST["meme-bot"]
            addMeme(searched_word, user, meme_img, top_text, bottom_text)
        return redirect("/" + searched_word)
    else:  # No se debería acceder a esto nunca, es por prevención
        return redirect("/login")


def page_xml(request):  # Devuelve xml de todas las palabras almacenadas
    content = Word.objects.all()
    context = {'word_list': content}

    return render(request, "MisPalabras/xml/words.xml", context, content_type="text/xml")


def page_json(request):  # Devuelve json de todas las palabras almacenadas
    words = Word.objects.values()
    data = list(words)
    return JsonResponse(data, safe=False)
# - - - - - - - - - - - - - -
# HOME methods
# - - - - - - - - - - - - - -

# Devuelve las palabras a mostrar en la tabla
def getTableWords(num: int):
    words_ordered = Word.objects.all().order_by("-date")
    start = 5 * num
    end = min(start + 5, len(words_ordered))
    return words_ordered[start:end]


# Devuelve el HTML necesario para mostrar la información de una palabra en la tabla de Home
def getWordHTML(word: Word):
    title = word.name.capitalize()
    title_link = "/" + title
    description = getWikiText(word.name)
    author = word.__author__()
    date = word.__date__()
    votes = word.votes
    img = getWikiImage(word.name)

    textHTML = WORD_HTML.format(word_title=title, word_title_link=title_link, word_description=description,
                                author=author, date=date, img=img,
                                votes=votes)
    return mark_safe(textHTML)


# Devuelve el HTML completo de la tabla de palabras
def getTableWordsHTML(num: int):
    words = getTableWords(num)
    textHTML = ""
    for word in words:
        textHTML += getWordHTML(word)

    return mark_safe(textHTML)


def getWordCommentsHTML(word: Word):
    comments = Comment.objects.all().filter(word=word)
    textHTML = ""
    for comment in comments:
        textHTML += COMMENT_HTML.format(author=comment.author, date=comment.__date__(), comment_text=comment.content)
    return mark_safe(textHTML)


def getNumberOfPages():
    number_of_words = len(Word.objects.all())
    number_of_pages = int(number_of_words / 5)
    if (number_of_words % 5) != 0:
        number_of_pages += 1
    return number_of_pages


# - - - - - - - - - - - - - -
# WORD methods
# - - - - - - - - - - - - - -

def addWord(word, user):
    name = str(word).lower()
    if not isAlreadyAdded(word):  # Comprobamos que la palabra todavía no está añadida
        wordObject = Word.objects.create(name=name, author=user)


def voteWord(word: str, user: User):
    name = word.lower()
    if not isAlreadyVoted(word, user):
        word = Word.objects.get(name=name)
        Vote.objects.create(author=user, word=word)
        word.votes = word.votes + 1
        word.save()


def removeVoteWord(word: str, user: User):
    name = word.lower()
    if isAlreadyVoted(word, user):
        word = Word.objects.get(name=name)
        Vote.objects.get(author=user, word=word).delete()
        word.votes = word.votes - 1
        word.save()


def addComment(word: str, user: User, text: str):
    name = word.lower()
    word = Word.objects.get(name=name)
    comment = Comment.objects.create(author=user, word=word, content=text)
    comment.save()


def addLink(word: str, user: User, link: str):
    name = word.lower()
    word = Word.objects.get(name=name)
    linkObject = Link.objects.create(word=word, author=user, url=link)

    addTarjetaEmbebida(linkObject)

    linkObject.save()


def addTarjetaEmbebida(link: Link):
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(link.url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlData = response.read().decode("UTF-8")
    soup = BeautifulSoup(htmlData, 'html.parser')
    # og:description
    ogDescription = soup.find("meta", propery="og:description")
    teDescription = None
    teImage = None
    if ogDescription:
        teDescription = ogDescription['content']
    # og:image
    ogImage = soup.find("meta", propery="og:title")
    if ogImage:
        teImage = ogImage['content']

    te = TarjetaEmbebida.objects.create(image=teImage, content=teDescription, link=link)
    te.save()


def addFlickr(word: str, user: User):
    word = Word.objects.all().get(name=word.lower())
    content = getFlickrImage(word)
    if not AutoInfo.objects.all().filter(word=word, author=user, content=content, site="Flickr").exists():
        AutoInfo.objects.create(word=word, author=user, content=content, site="Flickr")


def addRAE(word: str, user: User):
    word = Word.objects.all().get(name=word.lower())
    content = getRAEText(word)
    if not AutoInfo.objects.all().filter(word=word, author=user, content=content, site="RAE").exists():
        AutoInfo.objects.create(word=word, author=user, content=content, site="RAE")


def addMeme(word: str, user: User, img: str, top: str, bot: str):
    word = Word.objects.all().get(name=word.lower())
    content = getMemeImage(word, img, top, bot)
    if not AutoInfo.objects.all().filter(word=word, author=user, content=content, site="Meme").exists():
        AutoInfo.objects.create(word=word, author=user, content=content, site="Meme")


def getFlickrImage(word: Word):
    url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + str(word.name)
    flickr_response = requests.get(url)
    flickr_content = flickr_response.content
    flickr_XML_root = ET.fromstring(flickr_content)

    entry_tag = flickr_XML_root.find("{http://www.w3.org/2005/Atom}entry")
    link_tags = entry_tag.findall("{http://www.w3.org/2005/Atom}link")
    link_enclosure: {}
    for link in link_tags:
        if "enclosure" in link.attrib.values():
            return link.attrib["href"]

    print("Couldn't find Flickr image")
    return None


def getRAEText(word: Word) -> str:
    url = "https://dle.rae.es/" + str(word.name)
    # user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36"
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(url, headers=headers)
    with urllib.request.urlopen(req) as response:
        htmlData = response.read().decode("UTF-8")

    soup = BeautifulSoup(htmlData, 'html.parser')

    ogDescription = soup.find("meta", property="og:description")
    rae_text = ogDescription["content"]

    return rae_text


def getMemeImage(word: Word, img: str, top: str, bot: str) -> str:
    return "https://apimeme.com/meme?meme=" + img + "&top=" + top + "&bottom=" + bot

def getAIFlickrHTML(word: Word):
    if AutoInfo.objects.all().filter(word=word, site="Flickr").exists():
        ai = AutoInfo.objects.all().get(word=word, site="Flickr")
        content = ai.content
        textHTML = WORD_FLICKR.format(img_url=content)
    else:
        link = "/" + word.name + "/flickr"
        textHTML = WORD_ADD_FORM.format(link=link)

    return mark_safe(textHTML)

def getAIRAEHTML(word: Word):
    if AutoInfo.objects.all().filter(word=word, site="RAE").exists():
        ai = AutoInfo.objects.all().get(word=word, site="RAE")
        content = ai.content
        textHTML = WORD_RAE.format(definition=content)
        return mark_safe(textHTML)
    else:
        link = "/" + word.name + "/RAE"
        textHTML = WORD_ADD_FORM.format(link=link)

    return mark_safe(textHTML)


def getAIMemeHTML(word: Word):
    if AutoInfo.objects.all().filter(word=word, site="Meme").exists():
        ai = AutoInfo.objects.all().get(word=word, site="Meme")
        content = ai.content
        textHTML = WORD_MEME.format(img_url=content)
        return mark_safe(textHTML)
    else:
        link = "/" + word.name + "/meme"
        textHTML = WORD_MEME_FORM.format(link=link)

    return mark_safe(textHTML)


def isAlreadyAdded(word: str) -> bool:
    name = str(word).lower()
    return Word.objects.filter(name=name).exists()


def isAlreadyVoted(word: str, user) -> bool:
    name = str(word).lower()
    word = Word.objects.get(name=name)
    return Vote.objects.filter(author=user, word=word).exists()


def getWordButtonContext(word, user):
    name = str(word).lower()
    add_display = ""
    vote_display = ""
    rmvote_display = ""

    if isAlreadyAdded(word):  # Palabra ya añadida
        add_display = "display:none"
        if isAlreadyVoted(word, user):
            vote_display = "display:none"
        else:
            rmvote_display = "display:none"
    else:
        vote_display = "display:none"
        rmvote_display = "display:none"

    return add_display, vote_display, rmvote_display


def getAddedHTML(word: str) -> str:
    name = str(word).lower()
    dbWord = Word.objects.get(name=name)
    textHTML = "<div><p id='added-text'>"
    textHTML += "Añadido por " + dbWord.__author__() + " en " + dbWord.__date__()
    textHTML += "</p></div>"
    return mark_safe(textHTML)


def getWordCommentHTML(comment: Comment) -> str:
    author = comment.author
    date = comment.__date__()
    comment_text = comment.content
    textHTML = COMMENT_HTML.format(author=author, date=date, comment_text=comment_text)
    return mark_safe(textHTML)


def getWikiImage(word: str):
    wiki_image_response = requests.get(
        "https://es.wikipedia.org/w/api.php?action=query&titles=" + word + "&prop=pageimages&format=json&pithumbsize=200")
    wiki_image_content = wiki_image_response.content
    wiki_image_JSON = json.loads(wiki_image_content)

    claves = wiki_image_JSON["query"]["pages"].keys()
    page_id_dict = {}
    try:
        for key in claves:
            page_id_dict = wiki_image_JSON["query"]["pages"][key]
            return page_id_dict["thumbnail"]["source"]
    except KeyError:
        return None


def getWikiText(word: str):
    url = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + word + "&prop=extracts&exintro&explaintext"
    wiki_response = requests.get(url)
    wiki_content = wiki_response.content
    wiki_XML_root = ET.fromstring(wiki_content)  # ET es xml.etree.ElementTree

    try:
        for xml_node in wiki_XML_root.iter("extract"):
            word_info = xml_node.text  # Obtenemos la info de la palabra
            if word_info:
                return removeAnnotations(word_info)
    except KeyError:
        pass
    return "Couldn't find any information of this word."


# Para eliminar los [x] de Wikipedia
def removeAnnotations(description: str):
    return re.sub(r'\[.*?]', '', description)


# - - - - - - - - - - - - - -
# PROFILE methods
# - - - - - - - - - - - - - -

def getProfileWordsHTML(user: User):
    words = Word.objects.all().filter(author=user).order_by("-date")
    textHTML = ""
    for word in words:
        word_link = "/" + word.name
        name = word.name.capitalize()
        date = word.__date__()
        description = getWikiText(name)
        text = PROFILE_WORD.format(word=name, word_link=word_link, date=date, description=description)
        textHTML += mark_safe(text)

    return mark_safe(textHTML)


def getProfileCommentsHTML(user: User):
    comments = Comment.objects.all().filter(author=user).order_by("-date")
    textHTML = ""
    for comment in comments:
        text = comment.content
        word = comment.word.name.capitalize()
        word_link = "/" + comment.word.name
        date = comment.__date__()
        text = PROFILE_COMMENT.format(comment=text, word=word, word_link=word_link, date=date)
        textHTML += mark_safe(text)

    return mark_safe(textHTML)


def getProfileLinksHTML(user: User):
    links = Link.objects.all().filter(author=user).order_by("-date")
    if not links:
        return mark_safe("You haven't added any links")
    textHTML = ""
    for link in links:
        url = link.link
        word = link.word.name.capitalize()
        if link.tarjeta is None:
            text = PROFILE_LINK.format(link=url, word=word)
        else:
            text = link.tarjeta.content
            img = link.tarjeta.image
            text = PROFILE_LINK_TE.format(link=url, text=text, img_link=img, word=word)

        textHTML += mark_safe(text)

    return mark_safe(textHTML)


def getProfileAIHTML(user: User):
    ai = AutoInfo.objects.all().filter(author=user).order_by("-date")
    textHTML = ""
    for entry in ai:
        word = entry.word.name.capitalize()
        content: str
        site = entry.site
        if site == "Flickr":
            content = WORD_FLICKR.format(img_url=entry.content)
        elif site == "RAE":
            content = WORD_RAE.format(definition=entry.content)
        elif site == "Meme":
            content = WORD_MEME.format(img_url=entry.content)
        else:
            continue
        textHTML += PROFILE_AI.format(content=content, word=word)
    return mark_safe(textHTML)


# - - - - - - - - - - - - - -
# SIGN UP methods
# - - - - - - - - - - - - - -

def addUser(name, email, password):
    user = User.objects.create_user(username=name, email=email, password=password)


def check(name, email, password, password2):
    # Comprobar si existe ya un usuario con este nombre
    if User.objects.filter(username=name).exists():
        return 1
    # Comprobar si existe ya un usuario con este email
    elif User.objects.filter(email=email).exists():
        return 2
    # Comprobar si las contraseñas son idénticas
    elif password != password2:
        return 3
    else:
        return 0


def getErrorMessage(num):
    if num == 1:
        return "Username already in use"
    elif num == 2:
        return "Email already in use"
    elif num == 3:
        return "Passwords do not match"
    else:
        raise ValueError("Error code not valid (" + num + ")")


def getRandomIdentifier(length):
    characters = string.ascii_letters + string.digits
    random_id = ''.join(random.choice(characters) for i in range(length))
    return random_id


# - - - - - - - - - - - - - -
# General methods
# - - - - - - - - - - - - - -


def getTopWordsHTML() -> str:
    words_ordered = Word.objects.all().order_by("-votes")
    top_words = words_ordered[:min(10, len(words_ordered))]
    textHTML = "<table><thead><tr><th>TOP 10</th></tr></thead><tbody>"
    for i in range(0, len(top_words)):
        word_link = "/" + top_words[i].name
        textHTML += "<tr><td>"
        textHTML += "<a href='" + word_link + "'>" + top_words[i].name.capitalize() + " (" + str(top_words[i].votes) + ")</a>"
        textHTML += "</td></tr>"
    textHTML += "</body></table>"
    return mark_safe(textHTML)
