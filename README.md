
# Entrega practica
## Datos
* Nombre: José Matas Luque
* Titulación: Ingeniería de Tecnologías de Telecomunicación
* Despliegue (url): https://jmatas.pythonanywhere.com/
* Video básico (url): https://youtu.be/sKDCgOwPU8Q
* Video parte opcional (url): https://youtu.be/sKDCgOwPU8Q

## Cuenta Admin Site
* jmatas/Pantumaca16
## Cuentas usuarios
* pinocho93/Pinocho1
* mpuntoRajoy/Contraseña01

## Resumen parte obligatoria

La página nos permite añadir palabras al sitio de la cual podremos extraer automáticamente información que necesitemos.
En la página de inicio se nos muestran paginadas las palabras añadidas, en grupos de 5 en 5, de modo que aparezcan las 
añadidas más recientemente primero.

A la derecha tendremos una barra lateral en la mayoría de las páginas, desde la que podremos buscar palabras para acceder 
a su página o ver el top 10 de palabras más votadas.

Al buscar una palabra se nos redirige a la página de dicha palabra. Automáticamente busca su información de wikipedia si existe,
sino muestra mediante un mensaje que no se ha podido encontrar la palabra, o que solamente se ha encontrado texto y no una imagen. 
Si somos usuarios podemos realizar más acciones en la página, como puede ser añadir palabras, votar, añadir comentarios y fuentes de información...
Para iniciar sesión simplemente hacemos click arriba a la derecha. De igual modo podemos registrarnos si no tenemos todavía una cuenta.

Mediante la barra de navegación podemos desplazarnos por distintas páginas. XML y JSON nos mostrarán el listado de todas las palabras 
almacenadas en el sitio en su respectivo formato. 

En el apartado Ayuda se muestra una breve descripción del sitio, similar a este resumen, mientras que en el apartado Perfil se nos mostrará todo 
el contenido almacenado por nosotros en caso de haber iniciado sesión. Si no lo hemos hecho, se nos redirigirá a la página de inicio de sesión.

A lo largo de las diversas páginas lo normal es que al hacer click en el nombre de una palabra te lleve a la página de la palabra, con contadas excepciones.




## Lista partes opcionales
* Favicon
* Revocar voto
* CSS elaborado para asimilar la estética a un sitio Web2

## Notas:

Por alguna razón que no he logrado encontrar y/o solucionar el comportamiento de la página en PythonAnywhere puede variar ligeramente con respecto 
al código ejecutado directamente desde PyCharm. Estas cosas son principalmente diferencias estéticas, tal vez por no leer correctamente algún documento css.

Del mismo modo, el vídeo está grabado desde Google Chrome en Windows, simplemente porque he intentado grabarlo en Linux accediendo remotamente a los laboratorios 
de la universidad e iba demasiado lento, complicando la grabación.

Por último, he subido en un mismo vídeo todo porque la parte opcional se tarda en mostrar 5 segundos, y no consideraba que mereciera la pena
subir a YT un vídeo de 10 segundos máximo.